git clone - Clona o repositório do link especificado.
git add - Adiciona um arquivo ao repositório local.
git commit - Comenta a alteração que foi feita.
git pull - Atualiza o repositório local.
git push - Atualiza o repositório remoto.
git remote - Visualiza quais servidores remotos você configurou.
git init - Cria um repositório vazio ou reinicializa um existente.
git config - Configura sua identidade.